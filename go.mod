module gitlab.com/demonihin/asterisk-call-testing

go 1.12

require (
	github.com/CyCoreSystems/ari v4.8.4+incompatible
	github.com/google/uuid v1.1.1
	github.com/inconshreveable/log15 v0.0.0-20180818164646-67afb5ed74ec // indirect
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mattn/go-isatty v0.0.8 // indirect
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/pkg/errors v0.8.1
	github.com/prometheus/client_golang v0.9.3
	gopkg.in/yaml.v3 v3.0.0-20190502103701-55513cacd4ae
)
