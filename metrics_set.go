package main

import (
	"github.com/prometheus/client_golang/prometheus"
)

type MetricsSet struct {
	callCounters callOperationCounters
}

type callOperationCounters struct {
	startedJobs *prometheus.CounterVec

	completedJobs *prometheus.CounterVec

	hangupEvents *prometheus.CounterVec

	totalEvents *prometheus.CounterVec

	startedTests *prometheus.CounterVec

	completedTests *prometheus.CounterVec

	getChannelDataRequests *prometheus.CounterVec

	createChannelRequests *prometheus.CounterVec

	channelDestroyedEvents *prometheus.CounterVec

	stasisEndEvents *prometheus.CounterVec

	stasisStartEvents *prometheus.CounterVec

	sendDTMFRequests *prometheus.CounterVec

	startMOHRequests *prometheus.CounterVec

	stopMOHRequests *prometheus.CounterVec

	hangupRequests *prometheus.CounterVec
}

func (c *MetricsSet) RegisterGlobal() error {

	if err := prometheus.Register(c.callCounters.startedJobs); err != nil {
		return err
	}

	if err := prometheus.Register(c.callCounters.completedJobs); err != nil {
		return err
	}

	if err := prometheus.Register(c.callCounters.hangupEvents); err != nil {
		return err
	}

	if err := prometheus.Register(c.callCounters.totalEvents); err != nil {
		return err
	}

	if err := prometheus.Register(c.callCounters.startedTests); err != nil {
		return err
	}

	if err := prometheus.Register(c.callCounters.completedTests); err != nil {
		return err
	}

	if err := prometheus.Register(c.callCounters.getChannelDataRequests); err != nil {
		return err
	}

	if err := prometheus.Register(c.callCounters.createChannelRequests); err != nil {
		return err
	}

	if err := prometheus.Register(c.callCounters.channelDestroyedEvents); err != nil {
		return err
	}

	if err := prometheus.Register(c.callCounters.stasisEndEvents); err != nil {
		return err
	}

	if err := prometheus.Register(c.callCounters.stasisStartEvents); err != nil {
		return err
	}

	if err := prometheus.Register(c.callCounters.sendDTMFRequests); err != nil {
		return err
	}

	if err := prometheus.Register(c.callCounters.startMOHRequests); err != nil {
		return err
	}

	if err := prometheus.Register(c.callCounters.stopMOHRequests); err != nil {
		return err
	}

	if err := prometheus.Register(c.callCounters.hangupRequests); err != nil {
		return err
	}

	return nil
}

func NewMetricsSet(appNames ...string) *MetricsSet {

	const (
		Namespace = "asterisk"
		Subsystem = "testcalls"

		ApplicationLabelName = "application"
		StatusLabelName      = "status"
	)

	ms := &MetricsSet{

		callCounters: callOperationCounters{

			startedJobs: prometheus.NewCounterVec(prometheus.CounterOpts{
				Namespace: Namespace,
				Subsystem: Subsystem,
				Name:      "started_jobs",
			}, []string{ApplicationLabelName}),

			completedJobs: prometheus.NewCounterVec(prometheus.CounterOpts{
				Namespace: Namespace,
				Subsystem: Subsystem,
				Name:      "completed_jobs",
			}, []string{ApplicationLabelName}),

			hangupEvents: prometheus.NewCounterVec(prometheus.CounterOpts{
				Namespace: Namespace,
				Subsystem: Subsystem,
				Name:      "hangup_events",
			}, []string{ApplicationLabelName, "hangup_cause"}),

			totalEvents: prometheus.NewCounterVec(prometheus.CounterOpts{
				Namespace: Namespace,
				Subsystem: Subsystem,
				Name:      "total_events",
			}, []string{ApplicationLabelName}),

			startedTests: prometheus.NewCounterVec(prometheus.CounterOpts{
				Namespace: Namespace,
				Subsystem: Subsystem,
				Name:      "started_tests",
			}, []string{ApplicationLabelName}),

			completedTests: prometheus.NewCounterVec(prometheus.CounterOpts{
				Namespace: Namespace,
				Subsystem: Subsystem,
				Name:      "completed_tests",
			}, []string{ApplicationLabelName}),

			getChannelDataRequests: prometheus.NewCounterVec(prometheus.CounterOpts{
				Namespace: Namespace,
				Subsystem: Subsystem,
				Name:      "get_data_requests",
			}, []string{ApplicationLabelName, StatusLabelName}),

			createChannelRequests: prometheus.NewCounterVec(prometheus.CounterOpts{
				Namespace: Namespace,
				Subsystem: Subsystem,
				Name:      "create_channel_requests",
			}, []string{ApplicationLabelName, StatusLabelName}),

			channelDestroyedEvents: prometheus.NewCounterVec(prometheus.CounterOpts{
				Namespace: Namespace,
				Subsystem: Subsystem,
				Name:      "channel_destroyed_events",
			}, []string{ApplicationLabelName, "destroy_cause", "unexpected_channel_id", "leg"}),

			stasisEndEvents: prometheus.NewCounterVec(prometheus.CounterOpts{
				Namespace: Namespace,
				Subsystem: Subsystem,
				Name:      "stasis_end_events",
			}, []string{ApplicationLabelName, "unexpected_channel_id", "leg"}),

			stasisStartEvents: prometheus.NewCounterVec(prometheus.CounterOpts{
				Namespace: Namespace,
				Subsystem: Subsystem,
				Name:      "stasis_start_events",
			}, []string{ApplicationLabelName, "unexpected_channel_id", "leg"}),

			sendDTMFRequests: prometheus.NewCounterVec(prometheus.CounterOpts{
				Namespace: Namespace,
				Subsystem: Subsystem,
				Name:      "send_dtmf_requests",
			}, []string{ApplicationLabelName, StatusLabelName}),

			startMOHRequests: prometheus.NewCounterVec(prometheus.CounterOpts{
				Namespace: Namespace,
				Subsystem: Subsystem,
				Name:      "start_moh_requests",
			}, []string{ApplicationLabelName, StatusLabelName}),

			stopMOHRequests: prometheus.NewCounterVec(prometheus.CounterOpts{
				Namespace: Namespace,
				Subsystem: Subsystem,
				Name:      "stop_moh_requests",
			}, []string{ApplicationLabelName, StatusLabelName}),

			hangupRequests: prometheus.NewCounterVec(prometheus.CounterOpts{
				Namespace: Namespace,
				Subsystem: Subsystem,
				Name:      "hangup_requests",
			}, []string{ApplicationLabelName, StatusLabelName}),
		},
	}

	for _, app := range appNames {

		ms.callCounters.startedJobs.WithLabelValues(app).Add(0)

		ms.callCounters.completedJobs.WithLabelValues(app).Add(0)

		// Skipped because hangup causes might be different
		// ms.callCounters.hangupEvents.WithLabelValues(app, )

		ms.callCounters.totalEvents.WithLabelValues(app).Add(0)

		ms.callCounters.startedTests.WithLabelValues(app).Add(0)

		ms.callCounters.completedTests.WithLabelValues(app).Add(0)

		ms.callCounters.getChannelDataRequests.WithLabelValues(app, "ok").Add(0)
		ms.callCounters.getChannelDataRequests.WithLabelValues(app, "fail").Add(0)

		ms.callCounters.createChannelRequests.WithLabelValues(app, "ok").Add(0)
		ms.callCounters.createChannelRequests.WithLabelValues(app, "fail").Add(0)

		// Skipped because of very different causes
		// channelDestroyedEvents

		ms.callCounters.stasisEndEvents.WithLabelValues(app, "yes", "").Add(0)
		ms.callCounters.stasisEndEvents.WithLabelValues(app, "no", "1").Add(0)
		ms.callCounters.stasisEndEvents.WithLabelValues(app, "no", "2").Add(0)

		ms.callCounters.stasisStartEvents.WithLabelValues(app, "yes", "").Add(0)
		ms.callCounters.stasisStartEvents.WithLabelValues(app, "no", "1").Add(0)
		ms.callCounters.stasisStartEvents.WithLabelValues(app, "no", "2").Add(0)

		ms.callCounters.sendDTMFRequests.WithLabelValues(app, "ok").Add(0)
		ms.callCounters.sendDTMFRequests.WithLabelValues(app, "fail").Add(0)

		ms.callCounters.startMOHRequests.WithLabelValues(app, "ok").Add(0)
		ms.callCounters.stopMOHRequests.WithLabelValues(app, "fail").Add(0)

		ms.callCounters.hangupRequests.WithLabelValues(app, "ok").Add(0)
		ms.callCounters.hangupRequests.WithLabelValues(app, "fail").Add(0)

	}

	return ms
}

func (c *MetricsSet) CountJobStart(appName string) {
	c.callCounters.startedJobs.WithLabelValues(appName).Inc()
}

func (c *MetricsSet) CountJobCompleted(appName string) {
	c.callCounters.completedJobs.WithLabelValues(appName).Inc()
}

func (c *MetricsSet) CountCreateChannel(appName string, succeed bool) {

	var status string
	if succeed {
		status = "ok"
	} else {
		status = "fail"
	}

	c.callCounters.createChannelRequests.With(prometheus.Labels{
		"application": appName,
		"status":      status}).Inc()
}

func (c *MetricsSet) CountHangupEvent(appName, hangupCause string) {

	c.callCounters.hangupEvents.With(prometheus.Labels{
		"application":  appName,
		"hangup_cause": hangupCause}).Inc()
}

func (c *MetricsSet) CountEvent(appName string) {
	c.callCounters.totalEvents.WithLabelValues(appName).Inc()
}

func (c *MetricsSet) CountChannelDestroyedEvent(appName, destroyCause string) {
	c.callCounters.channelDestroyedEvents.With(prometheus.Labels{
		"application":           appName,
		"destroy_cause":         destroyCause,
		"unexpected_channel_id": "no",
		"leg":                   "1"}).Inc()
}

func (c *MetricsSet) CountChannelDestroyedLeg2Event(appName, destroyCause string) {
	c.callCounters.channelDestroyedEvents.With(prometheus.Labels{
		"application":           appName,
		"destroy_cause":         destroyCause,
		"unexpected_channel_id": "no",
		"leg":                   "2"}).Inc()
}

func (c *MetricsSet) CountChannelUnexpectedIDDestroyedEvent(appName, destroyCause string) {
	c.callCounters.channelDestroyedEvents.With(prometheus.Labels{
		"application":           appName,
		"destroy_cause":         destroyCause,
		"unexpected_channel_id": "yes",
		"leg":                   ""}).Inc()
}

func (c *MetricsSet) CountStasisEndEvent(appName string) {
	c.callCounters.stasisEndEvents.With(prometheus.Labels{
		"application":           appName,
		"leg":                   "1",
		"unexpected_channel_id": "no"}).Inc()
}

func (c *MetricsSet) CountStasisEndLeg2Event(appName string) {
	c.callCounters.stasisEndEvents.With(prometheus.Labels{
		"application":           appName,
		"leg":                   "2",
		"unexpected_channel_id": "no"}).Inc()
}

func (c *MetricsSet) CountUnexpectedChannelIDStasisEndEvent(appName string) {
	c.callCounters.stasisEndEvents.With(prometheus.Labels{
		"application":           appName,
		"leg":                   "",
		"unexpected_channel_id": "yes"}).Inc()
}

func (c *MetricsSet) CountStasisStartEvent(appName string) {
	c.callCounters.stasisStartEvents.With(prometheus.Labels{
		"application":           appName,
		"leg":                   "1",
		"unexpected_channel_id": "no"}).Inc()
}

func (c *MetricsSet) CountStasisStartLeg2Event(appName string) {
	c.callCounters.stasisStartEvents.With(prometheus.Labels{
		"application":           appName,
		"leg":                   "2",
		"unexpected_channel_id": "no"}).Inc()
}

func (c *MetricsSet) CountUnexpectedChannelIDStasisStartEvent(appName string) {
	c.callCounters.stasisStartEvents.With(prometheus.Labels{
		"application":           appName,
		"leg":                   "",
		"unexpected_channel_id": "yes"}).Inc()
}

func (c *MetricsSet) CountSendDTMF(appName string, succeed bool) {

	var status string
	if succeed {
		status = "ok"
	} else {
		status = "fail"
	}

	c.callCounters.sendDTMFRequests.With(prometheus.Labels{
		"application": appName,
		"status":      status,
	}).Inc()
}

func (c *MetricsSet) CountStartMOH(appName string, succeed bool) {

	var status string
	if succeed {
		status = "ok"
	} else {
		status = "fail"
	}

	c.callCounters.startMOHRequests.With(prometheus.Labels{
		"application": appName,
		"status":      status,
	}).Inc()
}

func (c *MetricsSet) CountStopMOH(appName string, succeed bool) {

	var status string
	if succeed {
		status = "ok"
	} else {
		status = "fail"
	}

	c.callCounters.stopMOHRequests.With(prometheus.Labels{
		"application": appName,
		"status":      status,
	}).Inc()
}

func (c *MetricsSet) CountCallTestStarted(appName string) {
	c.callCounters.startedTests.WithLabelValues(appName).Inc()
}

func (c *MetricsSet) CountCallTestCompleted(appName string) {
	c.callCounters.completedTests.WithLabelValues(appName).Inc()
}

func (c *MetricsSet) CountGetChannelData(appName string, succeed bool) {

	var status string
	if succeed {
		status = "ok"
	} else {
		status = "fail"
	}

	c.callCounters.getChannelDataRequests.With(prometheus.Labels{
		"application": appName,
		"status":      status,
	}).Inc()
}

func (c *MetricsSet) CountHangupRequest(appName string, succeed bool) {

	var status string
	if succeed {
		status = "ok"
	} else {
		status = "fail"
	}

	c.callCounters.hangupRequests.With(prometheus.Labels{
		"application": appName,
		"status":      status,
	}).Inc()
}
