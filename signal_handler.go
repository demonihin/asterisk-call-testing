package main

import (
	"context"
	"log"
	"os"
	"os/signal"
)

func InitSignalHandler(cf context.CancelFunc) {

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	go func() {
		s := <-ch
		log.Printf("InitSignalHandler: Got signal: %s", s.String())
		cf()
	}()
}
