package main

import (
	"database/sql"

	"context"

	"time"

	_ "github.com/mattn/go-sqlite3"
	"github.com/pkg/errors"
)

type SSSession struct {
	db      *sql.DB
	enabled bool
}

type CallIDInfo struct {
	CID            string
	CreateDateTime time.Time
}

type ActivatedChecker interface {
	IsActivated() bool
}

type StateDeleter interface {
	StateDeleteCallIDContext(ctx context.Context, CID string, closedByHangupRequest bool) error
	ActivatedChecker
}

type StateUser interface {
	StateDeleteCallIDContext(ctx context.Context, CID string, closedByHangupRequest bool) error
	StateGetNotFinishedCallsContext(context.Context) ([]CallIDInfo, error)
	StateSaveCallIDContext(ctx context.Context, CID string) error
	ActivatedChecker
}

func (s *SSSession) IsActivated() bool {
	return s.enabled
}

func (s *SSSession) StateSaveCallIDContext(ctx context.Context, CID string) (err error) {
	// Add
	tx, err := s.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
		} else {
			_ = tx.Rollback()
		}
	}()

	_, err = tx.ExecContext(ctx,
		"INSERT INTO calls (call_id, create_datetime, active) VALUES (?, CURRENT_TIMESTAMP, true);",
		CID)
	if err != nil {
		return err
	}

	return nil
}

func (s *SSSession) StateDeleteCallIDContext(ctx context.Context, CID string, closedByHangupRequest bool) error {
	tx, err := s.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
		} else {
			_ = tx.Rollback()
		}
	}()

	sqlRes, err := tx.ExecContext(ctx,
		"UPDATE calls set active = false, closed_by_hangup_request = ?, closed_at = ? WHERE call_id = ?;",
		closedByHangupRequest, time.Now(), CID)
	if err != nil {
		return err
	}

	rowsCount, err := sqlRes.RowsAffected()
	if err != nil {
		return err
	}
	if rowsCount == 0 {
		err = errors.New("Deleted 0 CallIDs")
		return err
	}

	return nil
}

func (s *SSSession) StateGetNotFinishedCallsContext(ctx context.Context) ([]CallIDInfo, error) {
	tx, err := s.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = tx.Rollback()
	}()

	rows, err := tx.QueryContext(ctx,
		"SELECT call_id, create_datetime FROM calls WHERE active = true;")
	if err != nil {
		return nil, err
	}
	defer func() {
		if pErr := rows.Close(); pErr != nil {
			panic(pErr)
		}
	}()

	result := make([]CallIDInfo, 0)
	for rows.Next() {

		var (
			cid         string
			cidDateTime time.Time
		)
		if err = rows.Scan(&cid, &cidDateTime); err != nil {
			return nil, err
		}

		result = append(result, CallIDInfo{CID: cid, CreateDateTime: cidDateTime})
	}

	return result, nil
}

// InitStateSaver - Initializes State storage
func StateInitContext(ctx context.Context, filepath string) (s *SSSession, err error) {
	db, err := sql.Open("sqlite3", filepath)
	if err != nil {
		return nil, err
	}

	// Create DB tables
	/*
		BEGIN TRANSACTION;
		CREATE TABLE IF NOT EXISTS `calls` (
			`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
			`call_id`	TEXT NOT NULL UNIQUE,
			`create_datetime` DATETIME,
			`active` BOOL,
			`closed_by_hangup_request` BOOL,
			`closed_at` DATETIME
		);
		CREATE UNIQUE INDEX IF NOT EXISTS `pk_id_index` ON `calls` (
			`id`
		);
		CREATE UNIQUE INDEX IF NOT EXISTS `call_id` ON `calls` (
			`call_id`
		);
		COMMIT;
	*/
	tx, err := db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return nil, err
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
		} else {
			_ = tx.Rollback()
		}
	}()

	_, err = tx.ExecContext(ctx, `
		CREATE TABLE IF NOT EXISTS "calls" (
			"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
			"call_id"	TEXT NOT NULL UNIQUE,
			"create_datetime" DATETIME,
			"active" BOOL,
			"closed_by_hangup_request" BOOL,
			"closed_at" DATETIME
		);
		CREATE UNIQUE INDEX IF NOT EXISTS "pk_id_index" ON "calls" (
			"id"
		);
		CREATE UNIQUE INDEX IF NOT EXISTS "call_id" ON "calls" (
			"call_id"
		);
	`)
	if err != nil {
		return nil, err
	}

	return &SSSession{
		db:      db,
		enabled: true,
	}, nil
}
