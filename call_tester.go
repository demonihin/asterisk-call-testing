package main

import (
	"context"
	"log"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/CyCoreSystems/ari"
	"github.com/google/uuid"
)

type WaitContext struct {
	sync.WaitGroup
}

func (w *WaitContext) Emit() <-chan bool {
	ch := make(chan bool)
	go func() {
		w.Wait()
		ch <- true
		close(ch)
	}()

	return ch
}

// callTester - Performs one Call test in infinite loop
func callTester(ctx context.Context,
	ariCl ari.Client,
	inTest DialTest,
	eventsSource chan ari.Event,
	metricsSet *MetricsSet,
	stateSaver StateUser,
	finishWG *sync.WaitGroup) {

	var (
		jobWait   WaitContext
		channelID string
		mux       sync.Mutex
	)

	testSettings := &inTest

	// Compute channels count.
	// Local enpoint creates two channels (Asterisk docs)
	var channelsWaitCount int
	if strings.Contains(strings.ToUpper(testSettings.Endpoint), "LOCAL") {
		channelsWaitCount = 2
	} else {
		channelsWaitCount = 1
	}

	// Events dispatcher
	go func() {
		for {
			select {
			case evt := <-eventsSource:

				switch typedEvent := evt.(type) {

				case (*ari.StasisStart):
					// Protect channelID
					mux.Lock()

					go handleStasisStart(
						typedEvent,
						channelID,
						metricsSet,
						testSettings,
						ariCl)

					mux.Unlock()

				case (*ari.StasisEnd):
					// Protect channelID
					mux.Lock()

					go handleStasisEnd(typedEvent, channelID, metricsSet)

					mux.Unlock()

				case (*ari.ChannelHangupRequest):
					// Update metrics
					metricsSet.CountHangupEvent(testSettings.AppName,
						strconv.FormatInt(int64(typedEvent.Cause), 10))

				case (*ari.ChannelDestroyed):
					// Protect channelID
					mux.Lock()

					go handleChannelDestroyedContext(ctx, channelID, metricsSet,
						typedEvent, &jobWait, testSettings, stateSaver)

					mux.Unlock()

				default:
					log.Printf("callTester: eventDispatcher: unexpected event %s %s",
						typedEvent.GetApplication(), typedEvent.GetType())
				}

			case <-ctx.Done():
				log.Printf("callTester: eventDispatcher: Exit on ctx.Done()")
				return
			}
		}
	}()

	// Channel initiator
	go func() {
		// Send "goroutine finished" to caller
		defer func() {
			finishWG.Done()
		}()

		for {
			select {

			// Wait for test check interval
			case <-time.NewTimer(testSettings.CheckInterval).C:

				// Update metrics
				metricsSet.CountJobStart(testSettings.AppName)

				// Set per test global channel ID
				// Protect channelID
				mux.Lock()
				channelID = uuid.New().String()
				mux.Unlock()

				// Open the channel
				if _, err := ariCl.Channel().Originate(
					nil, ari.OriginateRequest{
						App:       testSettings.AppName,
						Endpoint:  testSettings.Endpoint,
						ChannelID: channelID,
						Timeout:   int(testSettings.DialTimeout / time.Second),
					}); err != nil {

					log.Printf("dialerChecker: Error creating a new channel: %v", err)

					// Update metrics
					metricsSet.CountCreateChannel(testSettings.AppName, false)

					continue
				} else {

					if stateSaver.IsActivated() {
						// Protect channelID
						mux.Lock()
						// Update state DB
						if err := stateSaver.StateSaveCallIDContext(
							ctx, channelID); err != nil {

							log.Printf("dialerChecker: Error updating state DB: %v", err)
						}
						mux.Unlock()
					}

				}

				// Update metrics
				metricsSet.CountCreateChannel(testSettings.AppName, true)

				log.Printf("dialerChecker: created application: %s, channel %s, to %s",
					testSettings.AppName, channelID, testSettings.Endpoint)

				// Wait for completion
				jobWait.Add(channelsWaitCount)

				select {
				case <-jobWait.Emit():
					break
				case <-ctx.Done():
					log.Printf("dialerChecker: Exiting on ctx.Done() internal loop")
					return
				}

			// Exit event loop
			case <-ctx.Done():
				log.Printf("dialerChecker: Exiting on ctx.Done() external loop")
				return
			}
		}

	}()
}
