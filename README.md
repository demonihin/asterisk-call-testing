# Asterisk call testing

## What for

The application is intended to integrate Asterisk PBX (asterisk.org) with Prometheus metrics.
It performs periodic test calls to configured destinations and exports Prometheus format metrics with results.

The application starts test calls, starts Music on Hold, hangups the calls.
If the full test passes, the result is success.

## Configuration

```yaml
# Asterisk ARI and WebSocket connection settings
ari-configuration:
  user: user
  password: password
  wsURL: ws://192.168.56.108:8088/ari/events
  ariURL: http://192.168.56.108:8088/ari

# Path to save sqlite3 db with test call logs
# The DB is used to store test call information to allow the programm to
# hangup not-finished calls from previous start.
# For example: after a system or the programm fatal stop.
state-saving-configuration:
  path: state.sqlite3

# Test directions to test
tests:
    # Endpoint - call destination
  - endpoint: Local/1001@from-internal
    # app-name - unique application name to track Asterisk's events
    app-name: test-1001
    # How often perform the test
    check-interval: 5s
    # Dial timeout. After the timeout the call is threated as failed.
    timeout: 1s
    # Time to play music on hold
    call-duration: 5s
    # string which sets DTMF (internal number) which the test utility will
    # send to the endpoint to pass (for example) through incoming calls
    # voice menu
    dtmf: 12345
    # DTMF Asterisk options
    dtmfopts:
        # Wait before send the first digit
        time-before: 100ms
        # Interval between sending digits in dtmf
        interdigits-interval: 10ms
        # Time to send one digit
        digit-duration: 1s
        # Time to wait after the last digit before next step
        time-after: 5s

  - endpoint: Local/1002@from-internal
    app-name: test-1002
    check-interval: 5s

```

Example config for Prometheus is given in "prometheus.yml" file.
Example Grafana dashboard config is given in "Asterisk_Test_Calls_Grafana_board.json" file.

## ToDo

Add exporting RTPStat structure to Prometheus.
RTPStat has not been included to Asterisk build which is in FreePBX distro (latest) which I use for testing the programm