package main

import (
	"time"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

// TesterConfiguration - contains configuration for one testing instance
type TesterConfiguration struct {
	ARI                ARIConfiguration  `yaml:"ari-configuration"`
	Tests              []DialTest        `yaml:"tests"`
	StateSaverSettings StateSavingConfig `yaml:"state-saving-configuration"`
}

// StateSavingConfiguration - Contains settings for storing state
type StateSavingConfig struct {
	FilePath string `yaml:"path,omitempty"`
}

// ARIConfiguration - configuration with ARI connection parameters
type ARIConfiguration struct {
	Password     string `yaml:"password"`
	Username     string `yaml:"user"`
	WebsocketURL string `yaml:"wsURL"`
	URL          string `yaml:"ariURL"`
}

// DialTest - configuration of one test application
type DialTest struct {
	Endpoint      string        `yaml:"endpoint"`
	AppName       string        `yaml:"app-name"`
	CheckInterval time.Duration `yaml:"check-interval,omitempty"`
	DialTimeout   time.Duration `yaml:"timeout,omitempty"`
	CallDuration  time.Duration `yaml:"call-duration,omitempty"`
	DTMF          string        `yaml:"dtmf,omitempty"`
	DTMFOpts      DTMFOptions
}

func (dt *DialTest) UnmarshalYAML(value *yaml.Node) error {

	type rawDialTest DialTest

	rawData := rawDialTest(*NewDialTest())

	// Decode
	if err := value.Decode(&rawData); err != nil {
		return err
	}

	// Set result
	*dt = DialTest(rawData)

	return nil
}

// NewDialTest - default parameters for DialTest
func NewDialTest() *DialTest {
	return &DialTest{
		CheckInterval: 60 * time.Second,
		DialTimeout:   30 * time.Second,
		CallDuration:  5 * time.Second,
		DTMFOpts: DTMFOptions{
			TimeAfterLastDigit:    300 * time.Millisecond,
			IntervalBetweenDigits: 100 * time.Millisecond,
			OneDigitDuration:      100 * time.Millisecond,
			TimeBeforeFirstDigit:  300 * time.Millisecond,
		},
	}
}

// DTMFOptions - description of DTMF parameters for DialTest
type DTMFOptions struct {
	TimeBeforeFirstDigit  time.Duration `yaml:"time-before,omitempty"`
	IntervalBetweenDigits time.Duration `yaml:"interdigits-interval,omitempty"`
	OneDigitDuration      time.Duration `yaml:"digit-duration,omitempty"`
	TimeAfterLastDigit    time.Duration `yaml:"time-after,omitempty"`
}

// LoadYAMLConfiguration - performs loading and validating configuration from YAML
func LoadYAMLConfiguration(inConfig []byte) (*TesterConfiguration, error) {

	var configuration TesterConfiguration
	if err := yaml.Unmarshal(inConfig, &configuration); err != nil {
		return nil, err
	}

	// Check that application names are unique
	appsNames := make([]string, 0, len(configuration.Tests))
	for _, t := range configuration.Tests {

		app := t.AppName
		for _, a := range appsNames {
			if app == a {
				return nil, errors.Errorf("App name %s is not unique", app)
			}

			appsNames = append(appsNames, app)
		}
	}

	return &configuration, nil
}
