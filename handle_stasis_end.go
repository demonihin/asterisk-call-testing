package main

import (
	"log"

	"github.com/CyCoreSystems/ari"
)

func handleStasisEnd(
	typedEvent *ari.StasisEnd,
	expectedChannelID string,
	metricsSet *MetricsSet) {

	eventChannelID := typedEvent.Channel.GetID()

	switch eventChannelID {
	case expectedChannelID:
		log.Printf("Stasis end application %s, channel %s, handle leg 1",
			typedEvent.Application, typedEvent.Channel.GetID())

		// Update metrics
		metricsSet.CountStasisEndEvent(typedEvent.Application)

	case expectedChannelID + ";2":
		log.Printf("Stasis end application %s, channel %s, handle leg 2",
			typedEvent.Application, typedEvent.Channel.GetID())

		// Update metrics
		metricsSet.CountStasisEndLeg2Event(typedEvent.Application)

	default:
		log.Printf("Stasis end wrong channel ID event. Got: application %s, channel %s. Expected channel %s",
			typedEvent.Application, typedEvent.Channel.GetID(), expectedChannelID)

		// Update metrics
		metricsSet.CountUnexpectedChannelIDStasisEndEvent(typedEvent.Application)

	}
}
