package main

import (
	"context"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/CyCoreSystems/ari"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"log"

	"sync"

	"github.com/CyCoreSystems/ari/client/native"
)

const (
	ConfigFileName = "config.yaml"
)

func main() {

	// Create context to cancel all threads
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Init OS signal handler
	InitSignalHandler(cancel)

	// Load configuration
	configData, err := ioutil.ReadFile(ConfigFileName)
	if err != nil {
		log.Fatalf("Can not open and read configuration. Error: %v", err)
	}

	testerConfiguration, err := LoadYAMLConfiguration(configData)
	if err != nil {
		log.Fatalf("Can not parse configuration. Error: %v", err)
	}

	// State Saver init
	saver, err := StateInitContext(ctx,
		testerConfiguration.StateSaverSettings.FilePath)
	if err != nil {
		log.Fatalf("Can not connect to SQLite DB, Err: %v", err)
	}

	// Apps list
	apps := make([]string, 0, len(testerConfiguration.Tests))
	for _, t := range testerConfiguration.Tests {
		apps = append(apps, string(t.AppName))
	}

	// Connect to Asterisk
	ncl, err := native.Connect(&native.Options{
		Application:  strings.Join(apps, ","),
		Password:     testerConfiguration.ARI.Password,
		Username:     testerConfiguration.ARI.Username,
		WebsocketURL: testerConfiguration.ARI.WebsocketURL,
		URL:          testerConfiguration.ARI.URL,
	})
	if err != nil {
		log.Fatalf("Can not connect to ARI. Error: %v", err)
	}

	// Hangup stale calls
	if err := hangupUnfinishedCalls(ctx, saver, ncl); err != nil {
		log.Fatalln(err)
	}

	// Create Prometheus metrics
	metricsSet := NewMetricsSet(apps...)

	// Register metrics
	if err := metricsSet.RegisterGlobal(); err != nil {
		log.Fatalf("Can not register metrics: %v", err)
	}

	// Make Events per-application destinations
	eventPipes := make(map[string]chan ari.Event)
	for _, t := range testerConfiguration.Tests {
		eventPipes[t.AppName] = make(chan ari.Event)
	}

	// Activate event handler
	go eventDispatcher(ctx, ncl, metricsSet, eventPipes)

	// Run tests and register Prometheus metrics
	var finishWG sync.WaitGroup
	for _, t := range testerConfiguration.Tests {
		finishWG.Add(1)
		go callTester(ctx, ncl, t, eventPipes[t.AppName], metricsSet, saver, &finishWG)

	}

	// Serve metrics
	srv := &http.Server{Addr: ":8080"}
	http.Handle("/metrics", promhttp.Handler())

	go func() { log.Fatalln(srv.ListenAndServe()) }()

	// Wait for all tasks completion
	finishWG.Wait()
	log.Printf("Prometheus metrics server stopped with error code %v",
		srv.Shutdown(context.Background()))

	// close active calls
	if err := hangupUnfinishedCalls(context.Background(), saver, ncl); err != nil {
		log.Fatalf("Can not close not yet finished calls: %v", err)
	}

	// log.Fatalln(http.ListenAndServe(":8080", nil))
}
