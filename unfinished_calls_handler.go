package main

import (
	"context"

	"log"

	"github.com/CyCoreSystems/ari"
	"github.com/CyCoreSystems/ari/client/native"
	"github.com/pkg/errors"
)

func hangupUnfinishedCalls(ctx context.Context, callsGetter StateUser,
	astCl ari.Client) error {

	if !callsGetter.IsActivated() {
		return errors.New("StateGetter is not activated")
	}

	calls, err := callsGetter.StateGetNotFinishedCallsContext(ctx)
	if err != nil {
		return err
	}

	for _, c := range calls {
		log.Printf("Hanging up a call with ID: %s", c)

		ch := astCl.Channel().Get(
			&ari.Key{
				Kind: ari.ChannelKey,
				ID:   c.CID,
			},
		)

		if err := ch.Hangup(); err != nil {
			errCode := native.CodeFromError(err)

			// Channel was not found - does not exist already
			// On 404 - Set the Channel's state to not active and not closed by the
			// Hangup request
			if errCode == 404 {
				log.Printf("hangupUnfinishedCalls: Channel %s created at %v "+
					"was not found. It must have been closed earlier.",
					c.CID, c.CreateDateTime)

				if err := callsGetter.StateDeleteCallIDContext(ctx, c.CID, false); err != nil {
					return err
				}
			} else {
				log.Printf("Error hanging up a channel %s created at %v "+
					"from previous run. Error: %v", c.CID, c.CreateDateTime, err)
				return err
			}
		} else {
			// Channel was closed by the Hangup request
			log.Printf("hangupUnfinishedCalls: Channel %s created at %v "+
				"was hanged up.", c.CID, c.CreateDateTime)

			if err := callsGetter.StateDeleteCallIDContext(ctx, c.CID, true); err != nil {
				return err
			}
		}
	}

	return nil
}
