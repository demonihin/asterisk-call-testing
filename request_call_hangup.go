package main

import (
	"log"

	"github.com/CyCoreSystems/ari"
)

func requestCallHangup(h *ari.ChannelHandle,
	appName string, metricsSet *MetricsSet) error {

	// Request hangup
	// Update data
	data, err := h.Data()
	if err != nil {
		log.Printf("requestHangup: getting channel data  error: %v", err)

		// Update metrics
		metricsSet.CountGetChannelData(appName, false)

		return err
	}

	log.Printf("requestHangup: handle application %s, channel: %s, connected to: %s",
		appName, h.ID(), data.Connected.String())

	// Update metrics
	metricsSet.CountGetChannelData(appName, true)

	log.Printf("requestHangup: channel: %s state: %s", data.ID, data.State)

	if data.State != "Up" {
		return err
	}

	if err := h.Hangup(); err != nil {

		log.Printf(`requestHangup: request hangup error for	application %s, channel: %s, connected to: %s. Error: %v`,
			appName, data.Name, data.Connected.String(), err)

		// Update metrics
		metricsSet.CountHangupRequest(appName, false)

		return err
	}

	// Update metrics
	metricsSet.CountHangupRequest(appName, true)
	metricsSet.CountJobCompleted(appName)

	log.Printf(`requestHangup: request hangup ok for application %s, channel: %s, connected to: %s`,
		appName, data.Name, data.Connected.String())

	return nil
}
