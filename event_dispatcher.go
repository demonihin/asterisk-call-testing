package main

import (
	"context"
	"log"

	"github.com/CyCoreSystems/ari"
)

func eventDispatcher(
	ctx context.Context,
	cl ari.Client,
	metricsSet *MetricsSet,
	eventDestinations map[string]chan ari.Event) {

	allRequired := cl.Bus().Subscribe(
		nil,
		ari.Events.ChannelDestroyed,
		ari.Events.StasisStart,
		ari.Events.StasisEnd,
		ari.Events.ChannelHangupRequest)

	for {
		select {
		case e := <-allRequired.Events():
			// Update metrics
			metricsSet.CountEvent(e.GetApplication())

			// Non blocking processing
			go func() {
				// Send event to destination
				dch, ok := eventDestinations[string(e.GetApplication())]
				if !ok {
					log.Fatalf("eventHandler: Can not find destination for application %s",
						e.GetApplication())
				}

				// Send event to consumer
				dch <- e
			}()

		case <-ctx.Done():
			return
		}
	}
}
