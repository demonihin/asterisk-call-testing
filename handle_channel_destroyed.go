package main

import (
	"log"

	"context"

	"github.com/CyCoreSystems/ari"
)

type Doner interface {
	Done()
}

func handleChannelDestroyedContext(
	ctx context.Context,
	channelID string,
	metricsSet *MetricsSet,
	typedEvent *ari.ChannelDestroyed,
	waitGroupToFinishJob Doner,
	testSettings *DialTest,
	deleter StateDeleter) {

	eventChannelID := typedEvent.Channel.GetID()

	// Check for which channel the event came
	switch eventChannelID {
	case channelID:
		log.Printf("Destroyed channel leg 1: application %s, channel %s, reason: %s",
			typedEvent.Application, typedEvent.Channel.GetID(), typedEvent.CauseTxt)

		// Update metrics
		metricsSet.CountChannelDestroyedEvent(typedEvent.GetApplication(),
			typedEvent.CauseTxt)

		// Update database state
		if deleter.IsActivated() {
			if err := deleter.StateDeleteCallIDContext(ctx, channelID, true); err != nil {
				log.Printf("Can not update channel state in database: %v", err)
			}
		}

		// Stop waiting
		waitGroupToFinishJob.Done()

	case channelID + ";2":
		log.Printf("Destroyed channel leg 2: application %s, channel %s, reason %s",
			typedEvent.Application, typedEvent.Channel.GetID(), typedEvent.CauseTxt)

		// Update metrics
		metricsSet.CountChannelDestroyedLeg2Event(typedEvent.GetApplication(),
			typedEvent.CauseTxt)

		// Stop waiting
		waitGroupToFinishJob.Done()

	default:
		log.Printf("Destroyed wrong channel ID event: Got: application %s, channel %s. Expected channel %s",
			typedEvent.Application, eventChannelID, channelID)

		metricsSet.CountChannelUnexpectedIDDestroyedEvent(typedEvent.GetApplication(),
			typedEvent.CauseTxt)
	}
}
