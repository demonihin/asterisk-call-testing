package main

import (
	"log"
	"time"

	"github.com/CyCoreSystems/ari"
)

func handleStasisStart(
	typedEvent *ari.StasisStart,
	expectedChannelID string,
	metricsSet *MetricsSet,
	testSettings *DialTest,
	ariCl ari.Client) {

	eventChannelID := typedEvent.Channel.GetID()

	switch eventChannelID {
	case expectedChannelID:
		log.Printf("Stasis application %s, channel %s, handle leg 1",
			typedEvent.Application, typedEvent.Channel.GetID())

		// Update metrics
		metricsSet.CountStasisStartEvent(typedEvent.Application)

	case expectedChannelID + ";2":
		log.Printf("Stasis application %s, channel %s, handle leg 2",
			typedEvent.Application, typedEvent.Channel.GetID())

		// Update metrics
		metricsSet.CountStasisStartLeg2Event(typedEvent.Application)

	default:
		log.Printf("Stasis wrong channel ID event. Got: application %s, channel %s. Expected channel %s",
			typedEvent.Application, typedEvent.Channel.GetID(), expectedChannelID)

		// Update metrics
		metricsSet.CountUnexpectedChannelIDStasisStartEvent(typedEvent.Application)

	}

	oneCallTestHandler(ariCl,
		ariCl.Channel().Get(
			typedEvent.Key(
				ari.ChannelKey, typedEvent.Channel.ID,
			),
		),
		testSettings, metricsSet)

}

func oneCallTestHandler(
	cl ari.Client, h *ari.ChannelHandle,
	testSettings *DialTest, metricsSet *MetricsSet) {

	// Update metrics
	metricsSet.CountCallTestStarted(testSettings.AppName)

	defer requestCallHangup(h, testSettings.AppName, metricsSet)

	// Send DTMF if required
	if len(testSettings.DTMF) != 0 {

		if err := h.SendDTMF(
			testSettings.DTMF, &ari.DTMFOptions{
				After:    testSettings.DTMFOpts.TimeAfterLastDigit,
				Before:   testSettings.DTMFOpts.TimeBeforeFirstDigit,
				Between:  testSettings.DTMFOpts.IntervalBetweenDigits,
				Duration: testSettings.DTMFOpts.OneDigitDuration,
			}); err != nil {

			// Update metrics
			metricsSet.CountSendDTMF(testSettings.AppName, false)

			log.Printf("stasisCallHandler: Fail to send DTMF for application %s, channel %s",
				testSettings.AppName, h.ID())

			return
		}

		// Update metrics
		metricsSet.CountSendDTMF(testSettings.AppName, true)

	}

	// Start Music On Hold
	if err := h.MOH(""); err != nil {

		// Update metrics
		metricsSet.CountStartMOH(testSettings.AppName, false)

		log.Printf("stasisCallHandler: Fail to start MoH for application %s, channel %s",
			testSettings.AppName, h.ID())

		return
	}

	// Update metrics
	metricsSet.CountStartMOH(testSettings.AppName, true)

	log.Printf("stasisCallHandler: Started MoH for application %s, channel %s",
		testSettings.AppName, h.ID())

	// Wait for testSettings.callDuration
	time.Sleep(testSettings.CallDuration)

	// Stop MoH
	if err := h.StopMOH(); err != nil {

		// Update metrics
		metricsSet.CountStopMOH(testSettings.AppName, false)

		log.Printf("stasisCallHandler: Fail to stop MoH for application %s, channel %s",
			testSettings.AppName, h.ID())

		return
	}

	// Update metrics
	metricsSet.CountStopMOH(testSettings.AppName, true)

	log.Printf("stasisCallHandler: Stopped MoH for application %s, channel %s",
		testSettings.AppName, h.ID())

	// Update metrics
	metricsSet.CountCallTestCompleted(testSettings.AppName)

}
